﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace LaerMark_NMTkm21
{
    public partial class Main : Form
    {
    
        Color menuButtonSelectColor = Color.FromArgb(23, 32, 42);
        Color menuButtonUnSelectColor = Color.FromArgb(51, 54, 59);
        private NissanLaserMarkEntities db = new NissanLaserMarkEntities();
       
        List<string> ccr_list_q = new List<string>();
        DataTable dt_ccrconfig = new DataTable();
        public Main()
        {
            InitializeComponent();
            tabControl1.Appearance = TabAppearance.FlatButtons;
            tabControl1.ItemSize = new Size(0, 1);
            tabControl1.SizeMode = TabSizeMode.Fixed;
            create_header_automode();
            addwaitingprint_tolistq();
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 12F, FontStyle.Bold);
            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 12F, FontStyle.Bold);
            dataGridView2.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
  
            button_auto.BackColor = menuButtonSelectColor;
            button_semi.BackColor = menuButtonUnSelectColor;
            button_setting.BackColor = menuButtonUnSelectColor;
            
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            label3.Text = DateTime.Now.ToString();
            panel10.BackColor = Color.FromArgb(51, 54, 59);
            textBox15.BackColor = Color.FromArgb(51, 54, 59);
            timer1.Enabled = false;
            timer2.Enabled = true;
          //  panel10.BackColor = Color.FromArgb(0, 192, 0);
        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            panel10.BackColor = Color.FromArgb(0, 192, 0);
            textBox15.BackColor = Color.FromArgb(0, 192, 0);
            timer1.Enabled = true;
            timer2.Enabled = false;
            move_ccr();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(1);
            button_auto.BackColor = menuButtonUnSelectColor;
            button_semi.BackColor = menuButtonSelectColor;
            button_manual.BackColor = menuButtonUnSelectColor;
            button_setting.BackColor = menuButtonUnSelectColor;
            create_header_semiauto();
         
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(0);
            button_auto.BackColor = menuButtonSelectColor;
            button_semi.BackColor = menuButtonUnSelectColor;
            button_manual.BackColor = menuButtonUnSelectColor;
            button_setting.BackColor = menuButtonUnSelectColor;
            create_header_automode();
            addwaitingprint_tolistq();
        }

        private void Button_manual_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(2);
            button_auto.BackColor = menuButtonUnSelectColor;
            button_semi.BackColor = menuButtonUnSelectColor;
            button_manual.BackColor = menuButtonSelectColor;
            button_setting.BackColor = menuButtonUnSelectColor;
        }

        private void Button_setting_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(3);
            button_auto.BackColor = menuButtonUnSelectColor;
            button_semi.BackColor = menuButtonUnSelectColor;
            button_manual.BackColor = menuButtonUnSelectColor;
            button_setting.BackColor = menuButtonSelectColor;
        }

        private void Set_print_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(5);
            button_auto.BackColor = menuButtonUnSelectColor;
            button_semi.BackColor = menuButtonUnSelectColor;
            button_manual.BackColor = menuButtonUnSelectColor;
            button_setting.BackColor = menuButtonSelectColor;
        }

        private void Set_basemodel_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(6);
            button_auto.BackColor = menuButtonUnSelectColor;
            button_semi.BackColor = menuButtonUnSelectColor;
            button_manual.BackColor = menuButtonUnSelectColor;
            button_setting.BackColor = menuButtonSelectColor;
            fill_basemodel_table();
            fill_masterModel();
        }

        private void Button_datafrom_ccr_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(6);
            button_auto.BackColor = menuButtonUnSelectColor;
            button_semi.BackColor = menuButtonUnSelectColor;
            button_manual.BackColor = menuButtonUnSelectColor;
            button_setting.BackColor = menuButtonSelectColor;
        }

        private void Button_datafrom_internal_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(7);
            button_auto.BackColor = menuButtonUnSelectColor;
            button_semi.BackColor = menuButtonUnSelectColor;
            button_manual.BackColor = menuButtonUnSelectColor;
            button_setting.BackColor = menuButtonSelectColor;
        }

        private void Set_ccrconfig_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(4);
            button_auto.BackColor = menuButtonUnSelectColor;
            button_semi.BackColor = menuButtonUnSelectColor;
            button_manual.BackColor = menuButtonUnSelectColor;
            button_setting.BackColor = menuButtonSelectColor;
            fill_ccr_config_initial();
        }

        private void Set_maintenance_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(8);
            button_auto.BackColor = menuButtonUnSelectColor;
            button_semi.BackColor = menuButtonUnSelectColor;
            button_manual.BackColor = menuButtonUnSelectColor;
            button_setting.BackColor = menuButtonSelectColor;
        }
     

        #region ccr_file
        private void move_ccr ()
        {
            string[] sourcePath_ = Directory.GetFiles(@"C:\Users\PC0GYP5E\Desktop\Laser Mark NMT21\LaerMark_NMTkm21\CCR service","*.ACT");
      

            try
            {
                if (sourcePath_.Length > 0)
                {

                    string sourcePath = ConfigurationManager.AppSettings["pathSource"];
                    string targetPath = ConfigurationManager.AppSettings["pathDest"];
                    DirectoryInfo di = new DirectoryInfo(sourcePath);

                    var myfile = (from f in di.GetFiles() orderby f.CreationTime ascending select f).First();
                    string pathex = sourcePath + myfile;
                    string pathexp = targetPath + myfile;
                    if (File.Exists(pathex))
                    {
                     
                        File.Move(pathex, pathexp);
                        DateTime ReadTime = DateTime.Now;
                        list_ccr(1, pathexp);
                        ccr_log(ReadTime, pathexp);
                    }
                }

            }

            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void addwaitingprint_tolistq()
        {
            try
            {
                ccr_list_q.Clear();
                string sql = "";
                sql = "SELECT * FROM [NissanLaserMark].[dbo].[CCR_Log]";
                List<CCR_Log> log = db.CCR_Log.SqlQuery(sql).ToList();
                for(int i = 0; i < log.Count; i++)
                {
                    if(log[i].Print_Status == "Waiting")
                    {
                        ccr_list_q.Add(log[i].FilePath);
                    }
                }
                fill_automode();
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void list_ccr(int select ,string pathDe)
        {
            try
            {
                if(select == 0)
                {
                    ccr_list_q.Add(pathDe);
                    fill_automode();
                }
                if(select == 1)
                {
                    ccr_list_q.Add(pathDe);
                    fill_automode();
                }
                else
                {
                    ccr_list_q.Remove(pathDe);
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void ccr_log (DateTime ReadTime,string pathDes)
        {
            try
            {
                CCR_Log ccrLog = new CCR_Log();
                ccrLog.ReadTime = ReadTime;
                ccrLog.FilePath = pathDes;
                ccrLog.Print_Status = "Waiting";
                db.CCR_Log.Add(ccrLog);
                db.SaveChanges();
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void read_text_ccr ()
        {
            try
            {
                //  string filepath = @"C:\Users\PC0GYP5E\Desktop\Laser Mark NMT21\LaerMark_NMTkm21\CCR service";
                string[] filepath = Directory.GetFiles(@"C:\Users\PC0GYP5E\Desktop\Laser Mark NMT21\LaerMark_NMTkm21\CCR service", "*.ACT");
             //   if(File.Exists(filepath[0]))
              //  {
               //    StreamReader reader = new StreamReader(filepath[0]);
              //  }
                //  List<string> line = File.ReadAllLines(filepath[]).ToList();
                List<string> line = new List<string>();
                
                for(int i = 0; i<filepath.Length;i++)
                {
                    line.Add(File.ReadAllLines(filepath[i]).GetValue(0).ToString());
                    //  dataGridView1.Rows[i].Cells[1].Value = line[i];
                    dataGridView1.Rows.Add("",line[i]);

                }

            //    StreamReader se = new StreamReader("C:\Users\PC0GYP5E\Desktop\Laser Mark NMT21\LaerMark_NMTkm21\CCR service");
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        #endregion

        #region ccr_internal_config
        //fill ccr config grid view
        private void fill_ccr_config_initial ()
        {
            try
            {
                string sql = "";
                sql = "SELECT *  FROM [NissanLaserMark].[dbo].[CCR_config]";
                List<CCR_config> ccr_config = db.Database.SqlQuery<CCR_config>(sql).ToList();
                dataGridView3.Rows.Clear();
                dataGridView3.Columns[4].Visible = false;
                
                for(int i = 0; i < ccr_config.Count;i++)
                {
                    dataGridView3.Rows.Add(i+1, ccr_config[i].Param_Name, ccr_config[i].Start_Position, ccr_config[i].Length, ccr_config[i].Param_ID ,ccr_config[i].ShowinTable);
                }
               

            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void Ccr_bt_edit_Click(object sender, EventArgs e)
        {
            dataGridView3.Enabled = true;
            ccr_bt_edit.Visible = false;
            ccon_bt_save.Visible = true;
        }
        private void Ccon_bt_save_Click(object sender, EventArgs e)
        {
            dataGridView3.Enabled = false;
            ccr_bt_edit.Visible = true;
            ccon_bt_save.Visible = false;
            update_ccr_config();
        }
        private void dt_config()
        {
            dt_ccrconfig.Rows.Clear();
            dt_ccrconfig.Columns.Add("param_id");
            dt_ccrconfig.Columns.Add("param_name");
            dt_ccrconfig.Columns.Add("start_position");
            dt_ccrconfig.Columns.Add("length");
        }
        private void update_ccr_config ()
        {
            
            try
            {
                string sql = "";
                string parameter_name = "";
                int param_id = 0;
                int ccr_start = 0;
                int length_ccr = 0;
                bool check_table = false;
                //CCR_config dt_con = new CCR_config();
                sql = "SELECT * FROM [NissanLaserMark].[dbo].[CCR_config]";
                 List<CCR_config> dt_con = db.CCR_config.SqlQuery(sql).ToList();
                //var dt_con = db.CCR_config.ToList();
                // db.Database.ExecuteSqlCommand(sql);
                //db.SaveChanges();
                //   dt_config();
                if (dt_con.Count > 0)
                {
                    if ((dataGridView3.Rows.Count - 1 > dt_con.Count) )
                    {
                        for (int i = 0; i < dataGridView3.Rows.Count - 1; i++)
                        {
                            if (dataGridView3.Rows[i].Cells[4].Value == null) { param_id = 0; }
                            else if (dataGridView3.Rows[i].Cells[4].Value != null) { param_id = int.Parse(dataGridView3.Rows[i].Cells[0].Value.ToString()); }
                            if (dataGridView3.Rows[i].Cells[1].Value == null) { parameter_name = ""; }
                            else if (dataGridView3.Rows[i].Cells[1].Value != null) { parameter_name = dataGridView3.Rows[i].Cells[1].Value.ToString(); }
                            if (dataGridView3.Rows[i].Cells[2].Value == null) { ccr_start = 0; }
                            else if (dataGridView3.Rows[i].Cells[2].Value != null)
                            {
                                ccr_start = int.Parse(dataGridView3.Rows[i].Cells[2].Value.ToString());
                            }
                            if (dataGridView3.Rows[i].Cells[3].Value == null) { length_ccr = 0; }
                            else if (dataGridView3.Rows[i].Cells[3].Value != null)
                            {
                                length_ccr = int.Parse(dataGridView3.Rows[i].Cells[3].Value.ToString());
                            }
                            if (dataGridView3.Rows[i].Cells[5].Value == null) { check_table = false; }
                            else if (dataGridView3.Rows[i].Cells[5].Value != null)
                            {
                                check_table = Convert.ToBoolean(dataGridView3.Rows[i].Cells[5].Value.ToString());
                            }
                            if (parameter_name == "" && ccr_start == 0 && length_ccr == 0)
                            {
                                db.CCR_config.Remove(dt_con[i]);
                            }
                            if(dt_con.Count > i)
                            {
                                dt_con[i].Param_Name = parameter_name;
                                dt_con[i].Start_Position = ccr_start;
                                dt_con[i].Length = length_ccr;
                                dt_con[i].ShowinTable = check_table;
                                db.Entry(dt_con[i]).State = EntityState.Modified;
                            }
                            else
                            {
                                //test
                                CCR_config ccr = new CCR_config();
                           //     ccr.Param_ID = int.Parse(dataGridView3.Rows[i].Cells[0].Value.ToString());
                                ccr.Param_Name = dataGridView3.Rows[i].Cells[1].Value.ToString();
                                ccr.Start_Position = int.Parse(dataGridView3.Rows[i].Cells[2].Value.ToString());
                                ccr.Length = int.Parse(dataGridView3.Rows[i].Cells[3].Value.ToString());
                                ccr.ShowinTable = Convert.ToBoolean(dataGridView3.Rows[i].Cells[5].Value.ToString());
                                db.CCR_config.Add(ccr);
                            }
             
                        }
                        db.SaveChanges();
                        fill_ccr_config_initial();
                    }
                    else if (dt_con.Count > dataGridView3.Rows.Count - 1)
                    {
                        for( int i = 0; i < dt_con.Count; i++)
                        {
                            if (dataGridView3.Rows[i].Cells[4].Value == null) { param_id = 0 ; }
                            else if(dataGridView3.Rows[i].Cells[4].Value != null) { param_id = int.Parse(dataGridView3.Rows[i].Cells[0].Value.ToString()); }
                            if (dataGridView3.Rows[i].Cells[1].Value == null) { parameter_name = ""; }
                            else if (dataGridView3.Rows[i].Cells[1].Value != null) { parameter_name = dataGridView3.Rows[i].Cells[1].Value.ToString(); }
                            if (dataGridView3.Rows[i].Cells[2].Value == null) { ccr_start = 0; }
                            else if (dataGridView3.Rows[i].Cells[2].Value != null)
                            {
                                ccr_start = int.Parse(dataGridView3.Rows[i].Cells[2].Value.ToString());
                            }
                            if (dataGridView3.Rows[i].Cells[3].Value == null) { length_ccr = 0; }
                            else if (dataGridView3.Rows[i].Cells[3].Value != null)
                            {
                                length_ccr = int.Parse(dataGridView3.Rows[i].Cells[3].Value.ToString());
                            }
                            if (dataGridView3.Rows[i].Cells[5].Value == null) { check_table = false; }
                            else if (dataGridView3.Rows[i].Cells[5].Value != null)
                            {
                                check_table = Convert.ToBoolean(dataGridView3.Rows[i].Cells[5].Value.ToString());
                            }
                            if (parameter_name == "" && ccr_start == 0 && length_ccr == 0)
                            {
                                db.CCR_config.Remove(dt_con[i]);
                            }
                            if (dataGridView3.Rows[i].Cells[2].Value != null && dataGridView3.Rows[i].Cells[1].Value != null && param_id == dt_con[i].Param_ID && parameter_name == dt_con[i].Param_Name && ccr_start == dt_con[i].Start_Position && length_ccr == dt_con[i].Length)
                            {
                                //skip because ok
                            }
                            else if(dataGridView3.Rows[i].Cells[1].Value == null && dataGridView3.Rows[i].Cells[2].Value == null && dataGridView3.Rows[i].Cells[3].Value == null)
                            {
                                //skip because db reccord more than gridview
                                db.CCR_config.Remove(dt_con[i]);
                            }
                            else if((param_id == dt_con[i].Param_ID && dataGridView3.Rows[i].Cells[1].Value != null) && (parameter_name != dt_con[i].Param_Name || ccr_start != dt_con[i].Start_Position || length_ccr != dt_con[i].Length || check_table != dt_con[i].ShowinTable))
                            {
                                //edit data db following gridview
                                dt_con[i].Param_Name = parameter_name;
                                dt_con[i].Start_Position = ccr_start;
                                dt_con[i].Length = length_ccr;
                                dt_con[i].ShowinTable = check_table;
                                db.Entry(dt_con[i]).State = EntityState.Modified;
                            }
                        }
                        db.SaveChanges();
                        fill_ccr_config_initial();
                    }
                    else if(dt_con.Count == dataGridView3.Rows.Count-1)
                    {
                        for(int i = 0; i < dt_con.Count; i++)
                        {
                            if (dataGridView3.Rows[i].Cells[4].Value == null) { param_id = 0; }
                            else if (dataGridView3.Rows[i].Cells[4].Value != null) { param_id = int.Parse(dataGridView3.Rows[i].Cells[0].Value.ToString()); }
                            if (dataGridView3.Rows[i].Cells[1].Value == null) { parameter_name = ""; }
                            else if (dataGridView3.Rows[i].Cells[1].Value != null) { parameter_name = dataGridView3.Rows[i].Cells[1].Value.ToString(); }
                            if (dataGridView3.Rows[i].Cells[2].Value == null) { ccr_start = 0; }
                            else if (dataGridView3.Rows[i].Cells[2].Value != null)
                            {
                                ccr_start = int.Parse(dataGridView3.Rows[i].Cells[2].Value.ToString());
                            }
                            if (dataGridView3.Rows[i].Cells[3].Value == null) { length_ccr = 0; }
                            else if (dataGridView3.Rows[i].Cells[3].Value != null)
                            {
                                length_ccr = int.Parse(dataGridView3.Rows[i].Cells[3].Value.ToString());
                            }
                            if (dataGridView3.Rows[i].Cells[5].Value == null) { check_table = false; }
                            else if (dataGridView3.Rows[i].Cells[5].Value != null)
                            {
                                check_table = Convert.ToBoolean(dataGridView3.Rows[i].Cells[5].Value.ToString());
                            }
                            if (parameter_name == "" && ccr_start == 0 && length_ccr == 0)
                            {
                                db.CCR_config.Remove(dt_con[i]);
                            }
                            else if ((param_id == dt_con[i].Param_ID) || parameter_name !=dt_con[i].Param_Name || ccr_start != dt_con[i].Start_Position || length_ccr != dt_con[i].Length || check_table != dt_con[i].ShowinTable )
                            {
                               // dt_con[i].Param_ID = param_id;
                                dt_con[i].Param_Name = parameter_name;
                                dt_con[i].Start_Position = ccr_start;
                                dt_con[i].Length = length_ccr;
                                dt_con[i].ShowinTable = check_table;
                                db.Entry(dt_con[i]).State = EntityState.Modified;
                            }
                        }
                        db.SaveChanges();
                        fill_ccr_config_initial();
                    }
                    else
                    {
                        for (int i = 0; i < dataGridView3.Rows.Count - 1; i++)
                        {
                            CCR_config ccr = new CCR_config();
                            ccr.Param_ID = int.Parse(dataGridView3.Rows[i].Cells[0].Value.ToString());
                            ccr.Param_Name = dataGridView3.Rows[i].Cells[1].Value.ToString();
                            ccr.Start_Position = int.Parse(dataGridView3.Rows[i].Cells[2].Value.ToString());
                            ccr.Length = int.Parse(dataGridView3.Rows[i].Cells[3].Value.ToString());
                            ccr.ShowinTable = Convert.ToBoolean(dataGridView3.Rows[i].Cells[5].Value.ToString());
                            db.Entry(ccr).State = EntityState.Modified;
                        }
                        db.SaveChanges();
                        fill_ccr_config_initial();
                    }
                }
                else if(dt_con.Count <=0)
                {
                    for(int i = 0; i <  dataGridView3.Rows.Count -1; i++)
                    {
                        if(dataGridView3.Rows[i].Cells[1].Value != null)
                        {
                            CCR_config ccr = new CCR_config();
                            ccr.Param_ID = int.Parse(dataGridView3.Rows[i].Cells[0].Value.ToString());
                            ccr.Param_Name = dataGridView3.Rows[i].Cells[1].Value.ToString();
                            ccr.Start_Position = int.Parse(dataGridView3.Rows[i].Cells[2].Value.ToString());
                            ccr.Length = int.Parse(dataGridView3.Rows[i].Cells[3].Value.ToString());
                            ccr.ShowinTable = Convert.ToBoolean(dataGridView3.Rows[i].Cells[5].Value.ToString());
                            db.CCR_config.Add(ccr);
                        }
                    }
                    db.SaveChanges();
                    fill_ccr_config_initial();
                }

                
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }


        #endregion

        #region automode
        private int gridview_Width(int numofchar)
        {
            int maxWidth = 0, temp = 0;
            char[] ch = new char[numofchar *2];
            string st_ch = "";
            for(int i = 0; i < numofchar *2 ; i++)
            {
                ch[i]='O';
                st_ch += ch[i];
            }
            string a = ch.GetValue(0).ToString();
         //   foreach (var obj in ch)
          //  {
                temp = TextRenderer.MeasureText(st_ch, new Font("Tahoma", 9f)).Width;
              
                if (temp > maxWidth)
                {
                    maxWidth = temp;
                }
         //  }
            return maxWidth;
        }
        private void create_header_automode()
        {
            try
            {
                dataGridView1.Columns.Clear();
                int gridwidth = 0;
                string sql = "";
                sql = "SELECT *FROM [NissanLaserMark].[dbo].[CCR_config] ";
                List<CCR_config> hea = db.CCR_config.SqlQuery(sql).ToList();
                dataGridView1.Columns.Add("No", "No");
                for (int i = 0; i < hea.Count; i++)
                {
                    if (hea[i].ShowinTable == true)
                    {
                        dataGridView1.Columns.Add(hea[i].Param_Name, hea[i].Param_Name);
                        if (hea[i].Param_Name.Length > hea[i].Length)
                        {
                           gridwidth = gridview_Width(hea[i].Param_Name.Length);
                        }
                        else if(hea[i].Param_Name.Length < hea[i].Length.Value)
                        {
                          gridwidth = gridview_Width(hea[i].Length.Value);
                        }
                        dataGridView1.Columns[i+1].Width = gridwidth;
                    }
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void fill_automode()
        {
            try
            {

                dataGridView1.Rows.Clear();
                List<string> line = new List<string>();
                List<string> sub_line = new List<string>();
                string sql = "";
                sql = "SELECT *  FROM [NissanLaserMark].[dbo].[CCR_config]";
                List<CCR_config> ccr_con = db.CCR_config.SqlQuery(sql).ToList<CCR_config>();
                List<int> position_start = new List<int>();
                List<int> length_stop = new List<int>();
                position_start.Clear();
                length_stop.Clear();
                for (int i = 0; i < ccr_con.Count; i++)
                {
                    position_start.Add((int)ccr_con[i].Start_Position);
                    length_stop.Add((int)ccr_con[i].Length);
                }

                for (int i = 0; i < ccr_list_q.Count; i++)
                {
                    sub_line.Clear();
                    sub_line.Add(Convert.ToString(i + 1));
                    line.Add(File.ReadAllLines(ccr_list_q[i]).GetValue(0).ToString());
                    for (int j = 0; j < ccr_con.Count; j++)
                    {

                        if (ccr_con[j].ShowinTable == true)
                        {
                            sub_line.Add(line[i].Substring(position_start[j], length_stop[j]));
                        }
                    }
                    dataGridView1.Rows.Add(sub_line.ToArray()) ;
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }

        #endregion
        #region model base
        private void fill_basemodel_table ()
        {
            try
            {
                int gd_width = 0;
                string sql = "";
                sql = "SELECT * FROM [NissanLaserMark].[dbo].[CCR_config]";
                List<CCR_config> master_ccr = db.CCR_config.SqlQuery(sql).ToList();
                dataGridView6.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView6.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 12F,FontStyle.Bold);
                dataGridView6.Rows.Clear();
                dataGridView6.Columns.Clear();
                dataGridView6.Columns.Add("No", "No");
                dataGridView6.Columns.Add("print_format", "Print format");
                for (int i = 0; i < master_ccr.Count; i++)
                {
                    if (master_ccr[i].ShowinTable == true)
                    {
                        dataGridView6.Columns.Add(master_ccr[i].Param_Name, master_ccr[i].Param_Name);
                        if (master_ccr[i].Param_Name.Length > master_ccr[i].Length.Value)
                        {
                            gd_width = gridview_Width(master_ccr[i].Param_Name.Length);
                        }
                        else if (master_ccr[i].Param_Name.Length <= master_ccr[i].Length.Value)
                        {
                            gd_width = gridview_Width(master_ccr[i].Length.Value);
                        }
                        dataGridView6.Columns[i].Width = gd_width;
                    }
                }
    
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void Bm_bt_edit_Click(object sender, EventArgs e)
        {
            dataGridView6.Enabled = true;
            bm_bt_edit.Visible = false;
            bm_bt_save.Visible = true;
        }
        private void Bm_bt_save_Click(object sender, EventArgs e)
        {
            dataGridView6.Enabled = false;
            bm_bt_edit.Visible = true;
            bm_bt_save.Visible = false;
        }
        private void fill_masterModel()
        {
            string sql = "";
            string param_data = "";
            sql = "SELECT * FROM [NissanLaserMark].[dbo].[Master_CCR_Detail]";
            List<Master_CCR_Detail> master = db.Master_CCR_Detail.SqlQuery(sql).ToList();
            for(int i = 0; i < master.Count; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    param_data = master[i].Param_Data;
                    dataGridView6.Rows[j].Cells[i].Value = master[i].Param_Data;

                }
            }
        }
        private void update_ccr_detail()
        {
            try
            {
                string sql = "";
                int master_id = 0;
                int param_id = 0;
                string param_data = "";
               
                sql = "SELECT * FROM [NissanLaserMark].[dbo].[Master_CCR_Detail]";
                List<Master_CCR_Detail> master_update = db.Master_CCR_Detail.SqlQuery("sql").ToList();
                if(master_update.Count >0)
                {
                  //  if()
                }
                else
                {

                }
            }
            catch(Exception ab)
            {

            }
        }

        #endregion

        #region semi auto
        private void create_header_semiauto()
        {
            try
            {
                dataGridView2.Columns.Clear();
                dataGridView2.Rows.Clear();
                int gd_width = 0;
                string sql = "";
                sql = "SELECT * FROM [NissanLaserMark].[dbo].[CCR_config]";
                List<CCR_config> head = db.CCR_config.SqlQuery(sql).ToList();

                dataGridView2.Columns.Add("No", "No");
                for (int i = 0; i < head.Count; i++)
                {
                    if (head[i].ShowinTable == true)
                    {
                        
                        dataGridView2.Columns.Add(head[i].Param_Name, head[i].Param_Name);
                        if(head[i].Param_Name.Length > head[i].Length.Value)
                        {
                            gd_width = gridview_Width(head[i].Param_Name.Length);
                        }
                        else if(head[i].Param_Name.Length <= head[i].Length.Value)
                        {
                            gd_width = gridview_Width(head[i].Length.Value);
                        }
                        dataGridView2.Columns[i].Width = gd_width;
                    }
                }
            }
            catch(Exception ab)
            {
                MessageBox.Show(ab.Message);
            }

        }
        private void addwaitingprint_tolistq_semiauto()
        {
            try
            {
                ccr_list_q.Clear();
                string sql = "";
                sql = "SELECT * FROM [NissanLaserMark].[dbo].[CCR_Log]";
                List<CCR_Log> log = db.CCR_Log.SqlQuery(sql).ToList();
                for (int i = 0; i < log.Count; i++)
                {
                    if (log[i].Print_Status == "Waiting")
                    {
                        ccr_list_q.Add(log[i].FilePath);
                    }
                }
                for(int i = 0; i < log.Count; i++)
                {
                    if(log[i].Print_Status == "Printed")
                    {
                        ccr_list_q.Add(log[i].FilePath);
                    }
                }
                fill_semiauto();
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }
        }
        private void fill_semiauto()
        {
            try
            {

                dataGridView2.Rows.Clear();
                List<string> line = new List<string>();
                List<string> sub_line = new List<string>();
                string sql = "";
                sql = "SELECT *  FROM [NissanLaserMark].[dbo].[CCR_config]";
                List<CCR_config> ccr_con = db.CCR_config.SqlQuery(sql).ToList<CCR_config>();
                List<int> position_start = new List<int>();
                List<int> length_stop = new List<int>();
                position_start.Clear();
                length_stop.Clear();
                for (int i = 0; i < ccr_con.Count; i++)
                {
                    position_start.Add((int)ccr_con[i].Start_Position);
                    length_stop.Add((int)ccr_con[i].Length);
                }

                for (int i = 0; i < ccr_list_q.Count; i++)
                {
                    sub_line.Clear();
                    sub_line.Add(Convert.ToString(i + 1));
                    line.Add(File.ReadAllLines(ccr_list_q[i]).GetValue(0).ToString());
                    for (int j = 0; j < ccr_con.Count; j++)
                    {

                        if (ccr_con[j].ShowinTable == true)
                        {
                            sub_line.Add(line[i].Substring(position_start[j], length_stop[j]));
                        }
                    }
                    dataGridView2.Rows.Add(sub_line.ToArray());
                }
            }
            catch (Exception ab)
            {
                MessageBox.Show(ab.Message);
            }

        }

        #endregion
    }
}
