﻿namespace LaerMark_NMTkm21
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button_setting = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button_manual = new System.Windows.Forms.Button();
            this.button_semi = new System.Windows.Forms.Button();
            this.button_auto = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.autotab = new System.Windows.Forms.TabPage();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.semiautotab = new System.Windows.Forms.TabPage();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.btlog_search = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel32 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.panel27 = new System.Windows.Forms.Panel();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.panel29 = new System.Windows.Forms.Panel();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.manualtab = new System.Windows.Forms.TabPage();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.panel40 = new System.Windows.Forms.Panel();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.panel37 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.panel39 = new System.Windows.Forms.Panel();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.panel36 = new System.Windows.Forms.Panel();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.setting = new System.Windows.Forms.TabPage();
            this.panel41 = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.set_logccr = new System.Windows.Forms.Button();
            this.set_maintenance = new System.Windows.Forms.Button();
            this.set_basemodel = new System.Windows.Forms.Button();
            this.set_print = new System.Windows.Forms.Button();
            this.set_ccrconfig = new System.Windows.Forms.Button();
            this.ccr_internal = new System.Windows.Forms.TabPage();
            this.panel42 = new System.Windows.Forms.Panel();
            this.panel48 = new System.Windows.Forms.Panel();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.internal_no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.internal_param = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.internal_start = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.internal_length = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel49 = new System.Windows.Forms.Panel();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.panel46 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.ccr_bt_edit = new System.Windows.Forms.Button();
            this.ccon_bt_save = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.no_ccr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ccr_parameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ccr_start = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ccr_length = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.param_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel47 = new System.Windows.Forms.Panel();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.print_setting = new System.Windows.Forms.TabPage();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.printset_datablock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.printset_source = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.printset_dataselect = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.panel53 = new System.Windows.Forms.Panel();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.panel50 = new System.Windows.Forms.Panel();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.panel51 = new System.Windows.Forms.Panel();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.base_model_setting = new System.Windows.Forms.TabPage();
            this.panel44 = new System.Windows.Forms.Panel();
            this.button_datafrom_internal = new System.Windows.Forms.Button();
            this.panel55 = new System.Windows.Forms.Panel();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.bm_bt_save = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.bm_bt_edit = new System.Windows.Forms.Button();
            this.panel54 = new System.Windows.Forms.Panel();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.base_internal = new System.Windows.Forms.TabPage();
            this.panel56 = new System.Windows.Forms.Panel();
            this.button_datafrom_ccr = new System.Windows.Forms.Button();
            this.panel57 = new System.Windows.Forms.Panel();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.panel58 = new System.Windows.Forms.Panel();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.base_in_no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_vehicle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_eng = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_engcap = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_mission = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_axle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_gvwr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_gcw = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_gawr_fr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_option = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.base_in_print = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Maintenance = new System.Windows.Forms.TabPage();
            this.panel45 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.autotab.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.semiautotab.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel31.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.manualtab.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.panel40.SuspendLayout();
            this.panel37.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.setting.SuspendLayout();
            this.panel41.SuspendLayout();
            this.ccr_internal.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel48.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.panel49.SuspendLayout();
            this.panel46.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.panel47.SuspendLayout();
            this.print_setting.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel52.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.panel53.SuspendLayout();
            this.panel50.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.panel51.SuspendLayout();
            this.base_model_setting.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel54.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.base_internal.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel58.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            this.Maintenance.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(6, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(900, 69);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.buttonClose);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.buttonMinimize);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(287, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1633, 71);
            this.panel2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(951, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 25);
            this.label3.TabIndex = 0;
            this.label3.Text = "hh:mm:ss";
            this.label3.Click += new System.EventHandler(this.Label3_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(27)))), ((int)(((byte)(17)))));
            this.buttonClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.buttonClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(35)))), ((int)(((byte)(23)))));
            this.buttonClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(194)))), ((int)(((byte)(199)))));
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.buttonClose.ForeColor = System.Drawing.Color.White;
            this.buttonClose.Location = new System.Drawing.Point(1596, 22);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(25, 25);
            this.buttonClose.TabIndex = 31;
            this.buttonClose.Text = "X";
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(27)))), ((int)(((byte)(17)))));
            this.buttonMinimize.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.buttonMinimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(35)))), ((int)(((byte)(23)))));
            this.buttonMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(194)))), ((int)(((byte)(199)))));
            this.buttonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.buttonMinimize.ForeColor = System.Drawing.Color.White;
            this.buttonMinimize.Location = new System.Drawing.Point(1565, 22);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(25, 25);
            this.buttonMinimize.TabIndex = 32;
            this.buttonMinimize.Text = "_";
            this.buttonMinimize.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(1386, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 29);
            this.label1.TabIndex = 33;
            this.label1.Text = "_________";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel3.Controls.Add(this.textBox2);
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Location = new System.Drawing.Point(0, 1037);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1920, 44);
            this.panel3.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.White;
            this.textBox2.Location = new System.Drawing.Point(1463, 14);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(400, 24);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "NISSAN MOTOR (THAILAND) CO., LTD.";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(10, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(512, 24);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Laser Certification Label Machine @2020";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.ForeColor = System.Drawing.Color.White;
            this.textBox7.Location = new System.Drawing.Point(43, 173);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(67, 24);
            this.textBox7.TabIndex = 34;
            this.textBox7.Text = "PLC :";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.ForeColor = System.Drawing.Color.Black;
            this.textBox5.Location = new System.Drawing.Point(129, 27);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(147, 24);
            this.textBox5.TabIndex = 4;
            this.textBox5.Text = "CONNECTED";
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.ForeColor = System.Drawing.Color.White;
            this.textBox8.Location = new System.Drawing.Point(6, 226);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(113, 24);
            this.textBox8.TabIndex = 3;
            this.textBox8.Text = "PRINTER :";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.ForeColor = System.Drawing.Color.White;
            this.textBox4.Location = new System.Drawing.Point(43, 114);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(73, 24);
            this.textBox4.TabIndex = 3;
            this.textBox4.Text = "CCR :";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel5.Controls.Add(this.textBox3);
            this.panel5.Location = new System.Drawing.Point(0, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(293, 79);
            this.panel5.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.ForeColor = System.Drawing.Color.White;
            this.textBox3.Location = new System.Drawing.Point(26, 25);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(239, 24);
            this.textBox3.TabIndex = 0;
            this.textBox3.Text = "CONNECTION STATUS";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.ForeColor = System.Drawing.Color.White;
            this.textBox9.Location = new System.Drawing.Point(6, 277);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(106, 24);
            this.textBox9.TabIndex = 35;
            this.textBox9.Text = "CAMERA :";
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.Color.Red;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.Location = new System.Drawing.Point(129, 170);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(147, 24);
            this.textBox10.TabIndex = 36;
            this.textBox10.Text = "DISCONNECT";
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.Location = new System.Drawing.Point(129, 223);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(147, 24);
            this.textBox11.TabIndex = 37;
            this.textBox11.Text = "CONNECTED";
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel4.Controls.Add(this.textBox12);
            this.panel4.Controls.Add(this.textBox11);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.textBox10);
            this.panel4.Controls.Add(this.textBox9);
            this.panel4.Controls.Add(this.textBox4);
            this.panel4.Controls.Add(this.textBox8);
            this.panel4.Controls.Add(this.textBox7);
            this.panel4.Controls.Add(this.groupBox1);
            this.panel4.Location = new System.Drawing.Point(0, 694);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(293, 342);
            this.panel4.TabIndex = 2;
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.Location = new System.Drawing.Point(129, 280);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(147, 24);
            this.textBox12.TabIndex = 3;
            this.textBox12.Text = "CONNECTED";
            this.textBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Location = new System.Drawing.Point(0, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(293, 241);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel6.Controls.Add(this.button_setting);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.button_manual);
            this.panel6.Controls.Add(this.button_semi);
            this.panel6.Controls.Add(this.button_auto);
            this.panel6.Controls.Add(this.pictureBox1);
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(291, 607);
            this.panel6.TabIndex = 3;
            // 
            // button_setting
            // 
            this.button_setting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.button_setting.FlatAppearance.BorderSize = 0;
            this.button_setting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_setting.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_setting.ForeColor = System.Drawing.Color.White;
            this.button_setting.Location = new System.Drawing.Point(0, 510);
            this.button_setting.Name = "button_setting";
            this.button_setting.Size = new System.Drawing.Size(291, 80);
            this.button_setting.TabIndex = 8;
            this.button_setting.Text = "SETTING";
            this.button_setting.UseVisualStyleBackColor = false;
            this.button_setting.Click += new System.EventHandler(this.Button_setting_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel7.Controls.Add(this.label2);
            this.panel7.Location = new System.Drawing.Point(0, 196);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(293, 77);
            this.panel7.TabIndex = 7;
            // 
            // button_manual
            // 
            this.button_manual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.button_manual.FlatAppearance.BorderSize = 0;
            this.button_manual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_manual.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_manual.ForeColor = System.Drawing.Color.White;
            this.button_manual.Location = new System.Drawing.Point(0, 430);
            this.button_manual.Name = "button_manual";
            this.button_manual.Size = new System.Drawing.Size(291, 80);
            this.button_manual.TabIndex = 6;
            this.button_manual.Text = "MANUAL ";
            this.button_manual.UseVisualStyleBackColor = false;
            this.button_manual.Click += new System.EventHandler(this.Button_manual_Click);
            // 
            // button_semi
            // 
            this.button_semi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.button_semi.FlatAppearance.BorderSize = 0;
            this.button_semi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_semi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_semi.ForeColor = System.Drawing.Color.White;
            this.button_semi.Location = new System.Drawing.Point(0, 350);
            this.button_semi.Name = "button_semi";
            this.button_semi.Size = new System.Drawing.Size(291, 80);
            this.button_semi.TabIndex = 5;
            this.button_semi.Text = "SEMIAUTO ";
            this.button_semi.UseVisualStyleBackColor = false;
            this.button_semi.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button_auto
            // 
            this.button_auto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.button_auto.FlatAppearance.BorderSize = 0;
            this.button_auto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_auto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_auto.ForeColor = System.Drawing.Color.White;
            this.button_auto.Location = new System.Drawing.Point(0, 273);
            this.button_auto.Name = "button_auto";
            this.button_auto.Size = new System.Drawing.Size(291, 80);
            this.button_auto.TabIndex = 4;
            this.button_auto.Text = "AUTO ";
            this.button_auto.UseVisualStyleBackColor = false;
            this.button_auto.Click += new System.EventHandler(this.Button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::LaerMark_NMTkm21.Properties.Resources.logo_nmt;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(290, 195);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.panel8.Location = new System.Drawing.Point(0, 606);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(291, 90);
            this.panel8.TabIndex = 8;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.autotab);
            this.tabControl1.Controls.Add(this.semiautotab);
            this.tabControl1.Controls.Add(this.manualtab);
            this.tabControl1.Controls.Add(this.setting);
            this.tabControl1.Controls.Add(this.ccr_internal);
            this.tabControl1.Controls.Add(this.print_setting);
            this.tabControl1.Controls.Add(this.base_model_setting);
            this.tabControl1.Controls.Add(this.base_internal);
            this.tabControl1.Controls.Add(this.Maintenance);
            this.tabControl1.Location = new System.Drawing.Point(296, 77);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1624, 959);
            this.tabControl1.TabIndex = 0;
            // 
            // autotab
            // 
            this.autotab.Controls.Add(this.panel17);
            this.autotab.Location = new System.Drawing.Point(4, 22);
            this.autotab.Name = "autotab";
            this.autotab.Padding = new System.Windows.Forms.Padding(3);
            this.autotab.Size = new System.Drawing.Size(1616, 933);
            this.autotab.TabIndex = 0;
            this.autotab.Text = "auto";
            this.autotab.UseVisualStyleBackColor = true;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.panel17.Controls.Add(this.panel20);
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Controls.Add(this.panel9);
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(1616, 933);
            this.panel17.TabIndex = 1;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel20.Controls.Add(this.panel21);
            this.panel20.Controls.Add(this.dataGridView1);
            this.panel20.Location = new System.Drawing.Point(4, 204);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(888, 719);
            this.panel20.TabIndex = 1;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel21.Controls.Add(this.label5);
            this.panel21.Location = new System.Drawing.Point(3, 3);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(882, 72);
            this.panel21.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(18, 106);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(851, 572);
            this.dataGridView1.TabIndex = 0;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Location = new System.Drawing.Point(898, 204);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(715, 719);
            this.panel18.TabIndex = 1;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel19.Controls.Add(this.label6);
            this.panel19.Location = new System.Drawing.Point(3, 1);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(709, 72);
            this.panel19.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel9.Controls.Add(this.panel16);
            this.panel9.Controls.Add(this.panel14);
            this.panel9.Controls.Add(this.panel13);
            this.panel9.Controls.Add(this.panel12);
            this.panel9.Controls.Add(this.panel11);
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.panel15);
            this.panel9.Location = new System.Drawing.Point(0, 1);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1616, 198);
            this.panel9.TabIndex = 0;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel16.Controls.Add(this.textBox20);
            this.panel16.Controls.Add(this.pictureBox8);
            this.panel16.Location = new System.Drawing.Point(1294, 8);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(174, 175);
            this.panel16.TabIndex = 6;
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox20.ForeColor = System.Drawing.Color.White;
            this.textBox20.Location = new System.Drawing.Point(41, 149);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(100, 19);
            this.textBox20.TabIndex = 2;
            this.textBox20.Text = "Return";
            this.textBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::LaerMark_NMTkm21.Properties.Resources._return;
            this.pictureBox8.Location = new System.Drawing.Point(25, 5);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(133, 134);
            this.pictureBox8.TabIndex = 0;
            this.pictureBox8.TabStop = false;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel14.Controls.Add(this.textBox18);
            this.panel14.Controls.Add(this.pictureBox6);
            this.panel14.Location = new System.Drawing.Point(905, 8);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(174, 175);
            this.panel14.TabIndex = 4;
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.ForeColor = System.Drawing.Color.White;
            this.textBox18.Location = new System.Drawing.Point(35, 145);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(100, 19);
            this.textBox18.TabIndex = 1;
            this.textBox18.Text = "Cut";
            this.textBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::LaerMark_NMTkm21.Properties.Resources.cutting;
            this.pictureBox6.Location = new System.Drawing.Point(25, 5);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(133, 134);
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel13.Controls.Add(this.textBox17);
            this.panel13.Controls.Add(this.pictureBox5);
            this.panel13.Location = new System.Drawing.Point(699, 8);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(174, 175);
            this.panel13.TabIndex = 3;
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox17.ForeColor = System.Drawing.Color.White;
            this.textBox17.Location = new System.Drawing.Point(39, 145);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(100, 19);
            this.textBox17.TabIndex = 1;
            this.textBox17.Text = "Feed Cut";
            this.textBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::LaerMark_NMTkm21.Properties.Resources.manufacturing;
            this.pictureBox5.Location = new System.Drawing.Point(25, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(133, 134);
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel12.Controls.Add(this.textBox16);
            this.panel12.Controls.Add(this.pictureBox4);
            this.panel12.Location = new System.Drawing.Point(502, 8);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(174, 175);
            this.panel12.TabIndex = 2;
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox16.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.ForeColor = System.Drawing.Color.White;
            this.textBox16.Location = new System.Drawing.Point(39, 149);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(100, 19);
            this.textBox16.TabIndex = 3;
            this.textBox16.Text = "Checking";
            this.textBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::LaerMark_NMTkm21.Properties.Resources.list;
            this.pictureBox4.Location = new System.Drawing.Point(25, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(133, 134);
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel11.Controls.Add(this.textBox14);
            this.panel11.Controls.Add(this.pictureBox3);
            this.panel11.Location = new System.Drawing.Point(106, 8);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(174, 175);
            this.panel11.TabIndex = 1;
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.ForeColor = System.Drawing.Color.White;
            this.textBox14.Location = new System.Drawing.Point(37, 145);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(100, 19);
            this.textBox14.TabIndex = 1;
            this.textBox14.Text = "Home";
            this.textBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::LaerMark_NMTkm21.Properties.Resources.sydney_opera_house;
            this.pictureBox3.Location = new System.Drawing.Point(25, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(133, 134);
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.panel10.Controls.Add(this.textBox15);
            this.panel10.Controls.Add(this.pictureBox2);
            this.panel10.Location = new System.Drawing.Point(305, 8);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(174, 175);
            this.panel10.TabIndex = 0;
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.ForeColor = System.Drawing.Color.White;
            this.textBox15.Location = new System.Drawing.Point(35, 146);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(100, 19);
            this.textBox15.TabIndex = 2;
            this.textBox15.Text = "Printing";
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::LaerMark_NMTkm21.Properties.Resources.print__2_;
            this.pictureBox2.Location = new System.Drawing.Point(25, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(133, 134);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel15.Controls.Add(this.textBox19);
            this.panel15.Controls.Add(this.pictureBox7);
            this.panel15.Location = new System.Drawing.Point(1097, 8);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(174, 175);
            this.panel15.TabIndex = 5;
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox19.ForeColor = System.Drawing.Color.White;
            this.textBox19.Location = new System.Drawing.Point(38, 146);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(100, 19);
            this.textBox19.TabIndex = 2;
            this.textBox19.Text = "Pull Off";
            this.textBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::LaerMark_NMTkm21.Properties.Resources.new_release;
            this.pictureBox7.Location = new System.Drawing.Point(25, 5);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(133, 134);
            this.pictureBox7.TabIndex = 0;
            this.pictureBox7.TabStop = false;
            // 
            // semiautotab
            // 
            this.semiautotab.Controls.Add(this.panel30);
            this.semiautotab.Location = new System.Drawing.Point(4, 22);
            this.semiautotab.Name = "semiautotab";
            this.semiautotab.Padding = new System.Windows.Forms.Padding(3);
            this.semiautotab.Size = new System.Drawing.Size(1616, 933);
            this.semiautotab.TabIndex = 1;
            this.semiautotab.Text = "semiauto";
            this.semiautotab.UseVisualStyleBackColor = true;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.panel30.Controls.Add(this.panel33);
            this.panel30.Controls.Add(this.panel31);
            this.panel30.Controls.Add(this.panel22);
            this.panel30.Location = new System.Drawing.Point(0, 2);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1616, 931);
            this.panel30.TabIndex = 7;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel33.Controls.Add(this.panel34);
            this.panel33.Location = new System.Drawing.Point(896, 206);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(715, 719);
            this.panel33.TabIndex = 3;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel34.Controls.Add(this.label8);
            this.panel34.Location = new System.Drawing.Point(3, 1);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(709, 72);
            this.panel34.TabIndex = 0;
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel31.Controls.Add(this.groupBox2);
            this.panel31.Controls.Add(this.panel32);
            this.panel31.Controls.Add(this.dataGridView2);
            this.panel31.Location = new System.Drawing.Point(2, 205);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(888, 719);
            this.panel31.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.btlog_search);
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Controls.Add(this.dateTimePicker2);
            this.groupBox2.Controls.Add(this.dateTimePicker1);
            this.groupBox2.Controls.Add(this.textBox39);
            this.groupBox2.Controls.Add(this.textBox37);
            this.groupBox2.Controls.Add(this.textBox36);
            this.groupBox2.Controls.Add(this.textBox35);
            this.groupBox2.Controls.Add(this.textBox34);
            this.groupBox2.Controls.Add(this.textBox33);
            this.groupBox2.Controls.Add(this.textBox31);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(21, 96);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(851, 177);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Red;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(180, 119);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(134, 42);
            this.button5.TabIndex = 61;
            this.button5.Text = "Clear";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // btlog_search
            // 
            this.btlog_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btlog_search.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.btlog_search.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.btlog_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btlog_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btlog_search.ForeColor = System.Drawing.Color.White;
            this.btlog_search.Location = new System.Drawing.Point(25, 119);
            this.btlog_search.Name = "btlog_search";
            this.btlog_search.Size = new System.Drawing.Size(134, 42);
            this.btlog_search.TabIndex = 60;
            this.btlog_search.Text = "Search";
            this.btlog_search.UseVisualStyleBackColor = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(635, 134);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(114, 17);
            this.checkBox1.TabIndex = 21;
            this.checkBox1.Text = "Enabled DateTime";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(635, 92);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 20;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(635, 50);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 19;
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox39.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox39.ForeColor = System.Drawing.Color.White;
            this.textBox39.Location = new System.Drawing.Point(598, 92);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(40, 19);
            this.textBox39.TabIndex = 17;
            this.textBox39.Text = "To  :";
            // 
            // textBox37
            // 
            this.textBox37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox37.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox37.ForeColor = System.Drawing.Color.White;
            this.textBox37.Location = new System.Drawing.Point(538, 46);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(100, 19);
            this.textBox37.TabIndex = 15;
            this.textBox37.Text = "Date, From :";
            // 
            // textBox36
            // 
            this.textBox36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox36.Location = new System.Drawing.Point(379, 89);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(149, 22);
            this.textBox36.TabIndex = 14;
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox35.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox35.ForeColor = System.Drawing.Color.White;
            this.textBox35.Location = new System.Drawing.Point(333, 89);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(40, 19);
            this.textBox35.TabIndex = 13;
            this.textBox35.Text = "To  :";
            // 
            // textBox34
            // 
            this.textBox34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox34.Location = new System.Drawing.Point(379, 46);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(149, 22);
            this.textBox34.TabIndex = 12;
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox33.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox33.ForeColor = System.Drawing.Color.White;
            this.textBox33.Location = new System.Drawing.Point(258, 46);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(115, 19);
            this.textBox33.TabIndex = 11;
            this.textBox33.Text = "Trim No. From :";
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox31.ForeColor = System.Drawing.Color.White;
            this.textBox31.Location = new System.Drawing.Point(15, 46);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(73, 19);
            this.textBox31.TabIndex = 10;
            this.textBox31.Text = "Model   :";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(90, 46);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(152, 24);
            this.comboBox1.TabIndex = 9;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel32.Controls.Add(this.label7);
            this.panel32.Location = new System.Drawing.Point(3, 3);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(882, 72);
            this.panel32.TabIndex = 1;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(21, 300);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(851, 377);
            this.dataGridView2.TabIndex = 0;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Controls.Add(this.panel24);
            this.panel22.Controls.Add(this.panel25);
            this.panel22.Controls.Add(this.panel26);
            this.panel22.Controls.Add(this.panel27);
            this.panel22.Controls.Add(this.panel28);
            this.panel22.Controls.Add(this.panel29);
            this.panel22.Location = new System.Drawing.Point(1, 1);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(1616, 198);
            this.panel22.TabIndex = 1;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel23.Controls.Add(this.textBox23);
            this.panel23.Controls.Add(this.pictureBox9);
            this.panel23.Location = new System.Drawing.Point(1294, 8);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(174, 175);
            this.panel23.TabIndex = 6;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.ForeColor = System.Drawing.Color.White;
            this.textBox23.Location = new System.Drawing.Point(41, 149);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(100, 19);
            this.textBox23.TabIndex = 2;
            this.textBox23.Text = "Return";
            this.textBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::LaerMark_NMTkm21.Properties.Resources._return;
            this.pictureBox9.Location = new System.Drawing.Point(25, 5);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(133, 134);
            this.pictureBox9.TabIndex = 0;
            this.pictureBox9.TabStop = false;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel24.Controls.Add(this.textBox24);
            this.panel24.Controls.Add(this.pictureBox10);
            this.panel24.Location = new System.Drawing.Point(905, 8);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(174, 175);
            this.panel24.TabIndex = 4;
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox24.ForeColor = System.Drawing.Color.White;
            this.textBox24.Location = new System.Drawing.Point(35, 145);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(100, 19);
            this.textBox24.TabIndex = 1;
            this.textBox24.Text = "Cut";
            this.textBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::LaerMark_NMTkm21.Properties.Resources.cutting;
            this.pictureBox10.Location = new System.Drawing.Point(25, 5);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(133, 134);
            this.pictureBox10.TabIndex = 0;
            this.pictureBox10.TabStop = false;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel25.Controls.Add(this.textBox25);
            this.panel25.Controls.Add(this.pictureBox11);
            this.panel25.Location = new System.Drawing.Point(699, 8);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(174, 175);
            this.panel25.TabIndex = 3;
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox25.ForeColor = System.Drawing.Color.White;
            this.textBox25.Location = new System.Drawing.Point(39, 145);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(100, 19);
            this.textBox25.TabIndex = 1;
            this.textBox25.Text = "Feed Cut";
            this.textBox25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::LaerMark_NMTkm21.Properties.Resources.manufacturing;
            this.pictureBox11.Location = new System.Drawing.Point(25, 5);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(133, 134);
            this.pictureBox11.TabIndex = 0;
            this.pictureBox11.TabStop = false;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel26.Controls.Add(this.label12);
            this.panel26.Controls.Add(this.pictureBox12);
            this.panel26.Location = new System.Drawing.Point(502, 8);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(174, 175);
            this.panel26.TabIndex = 2;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::LaerMark_NMTkm21.Properties.Resources.list;
            this.pictureBox12.Location = new System.Drawing.Point(25, 5);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(133, 134);
            this.pictureBox12.TabIndex = 0;
            this.pictureBox12.TabStop = false;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel27.Controls.Add(this.label10);
            this.panel27.Controls.Add(this.pictureBox13);
            this.panel27.Location = new System.Drawing.Point(106, 8);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(174, 175);
            this.panel27.TabIndex = 1;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::LaerMark_NMTkm21.Properties.Resources.sydney_opera_house;
            this.pictureBox13.Location = new System.Drawing.Point(25, 5);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(133, 134);
            this.pictureBox13.TabIndex = 0;
            this.pictureBox13.TabStop = false;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.panel28.Controls.Add(this.label11);
            this.panel28.Controls.Add(this.pictureBox14);
            this.panel28.Location = new System.Drawing.Point(305, 8);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(174, 175);
            this.panel28.TabIndex = 0;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::LaerMark_NMTkm21.Properties.Resources.print__2_;
            this.pictureBox14.Location = new System.Drawing.Point(25, 5);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(133, 134);
            this.pictureBox14.TabIndex = 0;
            this.pictureBox14.TabStop = false;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel29.Controls.Add(this.textBox29);
            this.panel29.Controls.Add(this.pictureBox15);
            this.panel29.Location = new System.Drawing.Point(1097, 8);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(174, 175);
            this.panel29.TabIndex = 5;
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox29.ForeColor = System.Drawing.Color.White;
            this.textBox29.Location = new System.Drawing.Point(38, 146);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(100, 19);
            this.textBox29.TabIndex = 2;
            this.textBox29.Text = "Pull Off";
            this.textBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::LaerMark_NMTkm21.Properties.Resources.new_release;
            this.pictureBox15.Location = new System.Drawing.Point(25, 5);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(133, 134);
            this.pictureBox15.TabIndex = 0;
            this.pictureBox15.TabStop = false;
            // 
            // manualtab
            // 
            this.manualtab.Controls.Add(this.panel35);
            this.manualtab.Location = new System.Drawing.Point(4, 22);
            this.manualtab.Name = "manualtab";
            this.manualtab.Size = new System.Drawing.Size(1616, 933);
            this.manualtab.TabIndex = 2;
            this.manualtab.Text = "manual";
            this.manualtab.UseVisualStyleBackColor = true;
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.panel35.Controls.Add(this.panel38);
            this.panel35.Controls.Add(this.panel37);
            this.panel35.Controls.Add(this.panel36);
            this.panel35.Location = new System.Drawing.Point(0, 2);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(1616, 931);
            this.panel35.TabIndex = 0;
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel38.Controls.Add(this.pictureBox18);
            this.panel38.Controls.Add(this.panel40);
            this.panel38.Location = new System.Drawing.Point(979, 310);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(637, 625);
            this.panel38.TabIndex = 0;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Location = new System.Drawing.Point(38, 124);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(569, 409);
            this.pictureBox18.TabIndex = 1;
            this.pictureBox18.TabStop = false;
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel40.Controls.Add(this.textBox81);
            this.panel40.Location = new System.Drawing.Point(2, 2);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(632, 72);
            this.panel40.TabIndex = 3;
            // 
            // textBox81
            // 
            this.textBox81.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.textBox81.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox81.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox81.ForeColor = System.Drawing.Color.White;
            this.textBox81.Location = new System.Drawing.Point(232, 17);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(211, 24);
            this.textBox81.TabIndex = 0;
            this.textBox81.Text = "Camera Detection";
            this.textBox81.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel37.Controls.Add(this.button2);
            this.panel37.Controls.Add(this.button1);
            this.panel37.Controls.Add(this.groupBox4);
            this.panel37.Controls.Add(this.groupBox3);
            this.panel37.Controls.Add(this.panel39);
            this.panel37.Location = new System.Drawing.Point(1, 310);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(972, 621);
            this.panel37.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(207, 533);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(138, 51);
            this.button2.TabIndex = 6;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(38, 533);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 51);
            this.button1.TabIndex = 5;
            this.button1.Text = "Print";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pictureBox16);
            this.groupBox4.Controls.Add(this.comboBox2);
            this.groupBox4.Controls.Add(this.textBox80);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.groupBox4.Location = new System.Drawing.Point(15, 111);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(366, 357);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Print Format Setting";
            // 
            // pictureBox16
            // 
            this.pictureBox16.Location = new System.Drawing.Point(17, 45);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(327, 182);
            this.pictureBox16.TabIndex = 7;
            this.pictureBox16.TabStop = false;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(173, 283);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(171, 33);
            this.comboBox2.TabIndex = 6;
            // 
            // textBox80
            // 
            this.textBox80.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox80.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox80.ForeColor = System.Drawing.Color.White;
            this.textBox80.Location = new System.Drawing.Point(9, 286);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(161, 24);
            this.textBox80.TabIndex = 5;
            this.textBox80.Text = "Select Format  :";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox79);
            this.groupBox3.Controls.Add(this.textBox78);
            this.groupBox3.Controls.Add(this.textBox77);
            this.groupBox3.Controls.Add(this.textBox76);
            this.groupBox3.Controls.Add(this.textBox75);
            this.groupBox3.Controls.Add(this.textBox74);
            this.groupBox3.Controls.Add(this.textBox73);
            this.groupBox3.Controls.Add(this.textBox72);
            this.groupBox3.Controls.Add(this.textBox71);
            this.groupBox3.Controls.Add(this.textBox70);
            this.groupBox3.Controls.Add(this.textBox69);
            this.groupBox3.Controls.Add(this.textBox68);
            this.groupBox3.Controls.Add(this.textBox67);
            this.groupBox3.Controls.Add(this.textBox66);
            this.groupBox3.Controls.Add(this.textBox65);
            this.groupBox3.Controls.Add(this.textBox64);
            this.groupBox3.Controls.Add(this.textBox63);
            this.groupBox3.Controls.Add(this.textBox62);
            this.groupBox3.Controls.Add(this.textBox61);
            this.groupBox3.Controls.Add(this.textBox60);
            this.groupBox3.Controls.Add(this.textBox59);
            this.groupBox3.Controls.Add(this.textBox58);
            this.groupBox3.Controls.Add(this.textBox57);
            this.groupBox3.Controls.Add(this.textBox56);
            this.groupBox3.Controls.Add(this.textBox55);
            this.groupBox3.Controls.Add(this.textBox54);
            this.groupBox3.Controls.Add(this.textBox53);
            this.groupBox3.Controls.Add(this.textBox52);
            this.groupBox3.Controls.Add(this.textBox51);
            this.groupBox3.Controls.Add(this.textBox50);
            this.groupBox3.Controls.Add(this.textBox49);
            this.groupBox3.Controls.Add(this.textBox48);
            this.groupBox3.Controls.Add(this.textBox47);
            this.groupBox3.Controls.Add(this.textBox46);
            this.groupBox3.Controls.Add(this.textBox45);
            this.groupBox3.Controls.Add(this.textBox44);
            this.groupBox3.Controls.Add(this.textBox43);
            this.groupBox3.Controls.Add(this.textBox42);
            this.groupBox3.Controls.Add(this.textBox41);
            this.groupBox3.Controls.Add(this.textBox40);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.groupBox3.Location = new System.Drawing.Point(398, 111);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(546, 476);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data Block";
            // 
            // textBox79
            // 
            this.textBox79.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox79.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox79.ForeColor = System.Drawing.Color.White;
            this.textBox79.Location = new System.Drawing.Point(293, 422);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(63, 24);
            this.textBox79.TabIndex = 42;
            this.textBox79.Text = "20";
            this.textBox79.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox78
            // 
            this.textBox78.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox78.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox78.ForeColor = System.Drawing.Color.White;
            this.textBox78.Location = new System.Drawing.Point(293, 379);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(63, 24);
            this.textBox78.TabIndex = 41;
            this.textBox78.Text = "19";
            this.textBox78.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox77
            // 
            this.textBox77.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox77.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox77.ForeColor = System.Drawing.Color.White;
            this.textBox77.Location = new System.Drawing.Point(293, 345);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(63, 24);
            this.textBox77.TabIndex = 40;
            this.textBox77.Text = "18";
            this.textBox77.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox76
            // 
            this.textBox76.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox76.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox76.ForeColor = System.Drawing.Color.White;
            this.textBox76.Location = new System.Drawing.Point(293, 307);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(63, 24);
            this.textBox76.TabIndex = 39;
            this.textBox76.Text = "17";
            this.textBox76.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox75
            // 
            this.textBox75.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox75.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox75.ForeColor = System.Drawing.Color.White;
            this.textBox75.Location = new System.Drawing.Point(293, 270);
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(63, 24);
            this.textBox75.TabIndex = 38;
            this.textBox75.Text = "16";
            this.textBox75.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox74
            // 
            this.textBox74.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox74.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox74.ForeColor = System.Drawing.Color.White;
            this.textBox74.Location = new System.Drawing.Point(293, 233);
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(63, 24);
            this.textBox74.TabIndex = 37;
            this.textBox74.Text = "15";
            this.textBox74.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox73
            // 
            this.textBox73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox73.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox73.ForeColor = System.Drawing.Color.White;
            this.textBox73.Location = new System.Drawing.Point(293, 195);
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(63, 24);
            this.textBox73.TabIndex = 36;
            this.textBox73.Text = "14";
            this.textBox73.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox72
            // 
            this.textBox72.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox72.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox72.ForeColor = System.Drawing.Color.White;
            this.textBox72.Location = new System.Drawing.Point(293, 158);
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(63, 24);
            this.textBox72.TabIndex = 35;
            this.textBox72.Text = "13";
            this.textBox72.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox71
            // 
            this.textBox71.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox71.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox71.ForeColor = System.Drawing.Color.White;
            this.textBox71.Location = new System.Drawing.Point(293, 124);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(63, 24);
            this.textBox71.TabIndex = 34;
            this.textBox71.Text = "12";
            this.textBox71.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox70
            // 
            this.textBox70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox70.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox70.ForeColor = System.Drawing.Color.White;
            this.textBox70.Location = new System.Drawing.Point(293, 87);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(63, 24);
            this.textBox70.TabIndex = 33;
            this.textBox70.Text = "11";
            this.textBox70.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox69
            // 
            this.textBox69.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox69.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox69.ForeColor = System.Drawing.Color.White;
            this.textBox69.Location = new System.Drawing.Point(20, 422);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(63, 24);
            this.textBox69.TabIndex = 32;
            this.textBox69.Text = "10";
            this.textBox69.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox68
            // 
            this.textBox68.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox68.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox68.ForeColor = System.Drawing.Color.White;
            this.textBox68.Location = new System.Drawing.Point(20, 385);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(63, 24);
            this.textBox68.TabIndex = 31;
            this.textBox68.Text = "9";
            this.textBox68.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox67
            // 
            this.textBox67.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox67.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox67.ForeColor = System.Drawing.Color.White;
            this.textBox67.Location = new System.Drawing.Point(20, 345);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(63, 24);
            this.textBox67.TabIndex = 30;
            this.textBox67.Text = "8";
            this.textBox67.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox66
            // 
            this.textBox66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox66.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox66.ForeColor = System.Drawing.Color.White;
            this.textBox66.Location = new System.Drawing.Point(20, 307);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(63, 24);
            this.textBox66.TabIndex = 29;
            this.textBox66.Text = "7";
            this.textBox66.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox65
            // 
            this.textBox65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox65.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox65.ForeColor = System.Drawing.Color.White;
            this.textBox65.Location = new System.Drawing.Point(20, 270);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(63, 24);
            this.textBox65.TabIndex = 28;
            this.textBox65.Text = "6";
            this.textBox65.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox64
            // 
            this.textBox64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox64.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox64.ForeColor = System.Drawing.Color.White;
            this.textBox64.Location = new System.Drawing.Point(20, 233);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(63, 24);
            this.textBox64.TabIndex = 27;
            this.textBox64.Text = "5";
            this.textBox64.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox63
            // 
            this.textBox63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox63.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox63.ForeColor = System.Drawing.Color.White;
            this.textBox63.Location = new System.Drawing.Point(20, 196);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(63, 24);
            this.textBox63.TabIndex = 26;
            this.textBox63.Text = "4";
            this.textBox63.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox62
            // 
            this.textBox62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox62.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox62.ForeColor = System.Drawing.Color.White;
            this.textBox62.Location = new System.Drawing.Point(20, 161);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(63, 24);
            this.textBox62.TabIndex = 25;
            this.textBox62.Text = "3";
            this.textBox62.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox61
            // 
            this.textBox61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox61.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox61.ForeColor = System.Drawing.Color.White;
            this.textBox61.Location = new System.Drawing.Point(20, 124);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(63, 24);
            this.textBox61.TabIndex = 24;
            this.textBox61.Text = "2";
            this.textBox61.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox60
            // 
            this.textBox60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.textBox60.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox60.ForeColor = System.Drawing.Color.White;
            this.textBox60.Location = new System.Drawing.Point(20, 87);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(63, 24);
            this.textBox60.TabIndex = 23;
            this.textBox60.Text = "1";
            this.textBox60.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(362, 419);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(174, 31);
            this.textBox59.TabIndex = 22;
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(362, 379);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(174, 31);
            this.textBox58.TabIndex = 21;
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(362, 342);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(174, 31);
            this.textBox57.TabIndex = 20;
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(362, 307);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(174, 31);
            this.textBox56.TabIndex = 19;
            // 
            // textBox55
            // 
            this.textBox55.Location = new System.Drawing.Point(362, 270);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(174, 31);
            this.textBox55.TabIndex = 18;
            // 
            // textBox54
            // 
            this.textBox54.Location = new System.Drawing.Point(362, 233);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(174, 31);
            this.textBox54.TabIndex = 17;
            // 
            // textBox53
            // 
            this.textBox53.Location = new System.Drawing.Point(362, 195);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(174, 31);
            this.textBox53.TabIndex = 16;
            // 
            // textBox52
            // 
            this.textBox52.Location = new System.Drawing.Point(362, 158);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(174, 31);
            this.textBox52.TabIndex = 15;
            // 
            // textBox51
            // 
            this.textBox51.Location = new System.Drawing.Point(362, 121);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(174, 31);
            this.textBox51.TabIndex = 14;
            // 
            // textBox50
            // 
            this.textBox50.Location = new System.Drawing.Point(362, 84);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(174, 31);
            this.textBox50.TabIndex = 13;
            // 
            // textBox49
            // 
            this.textBox49.Location = new System.Drawing.Point(101, 419);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(174, 31);
            this.textBox49.TabIndex = 12;
            // 
            // textBox48
            // 
            this.textBox48.Location = new System.Drawing.Point(101, 382);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(174, 31);
            this.textBox48.TabIndex = 11;
            // 
            // textBox47
            // 
            this.textBox47.Location = new System.Drawing.Point(101, 345);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(174, 31);
            this.textBox47.TabIndex = 10;
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(101, 307);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(174, 31);
            this.textBox46.TabIndex = 9;
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(101, 270);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(174, 31);
            this.textBox45.TabIndex = 8;
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(101, 233);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(174, 31);
            this.textBox44.TabIndex = 7;
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(101, 196);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(174, 31);
            this.textBox43.TabIndex = 6;
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(101, 158);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(174, 31);
            this.textBox42.TabIndex = 5;
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(101, 121);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(174, 31);
            this.textBox41.TabIndex = 4;
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(101, 84);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(174, 31);
            this.textBox40.TabIndex = 3;
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel39.Controls.Add(this.textBox38);
            this.panel39.Location = new System.Drawing.Point(1, 3);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(968, 72);
            this.panel39.TabIndex = 2;
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.textBox38.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox38.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox38.ForeColor = System.Drawing.Color.White;
            this.textBox38.Location = new System.Drawing.Point(336, 16);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(211, 24);
            this.textBox38.TabIndex = 0;
            this.textBox38.Text = "Printer Programe";
            this.textBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel36.Controls.Add(this.pictureBox17);
            this.panel36.Location = new System.Drawing.Point(1, 1);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(1616, 303);
            this.panel36.TabIndex = 2;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Location = new System.Drawing.Point(27, 22);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(1549, 256);
            this.pictureBox17.TabIndex = 8;
            this.pictureBox17.TabStop = false;
            // 
            // setting
            // 
            this.setting.Controls.Add(this.panel41);
            this.setting.Location = new System.Drawing.Point(4, 22);
            this.setting.Name = "setting";
            this.setting.Size = new System.Drawing.Size(1616, 933);
            this.setting.TabIndex = 3;
            this.setting.Text = "setting";
            this.setting.UseVisualStyleBackColor = true;
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.panel41.Controls.Add(this.button11);
            this.panel41.Controls.Add(this.button10);
            this.panel41.Controls.Add(this.button9);
            this.panel41.Controls.Add(this.set_logccr);
            this.panel41.Controls.Add(this.set_maintenance);
            this.panel41.Controls.Add(this.set_basemodel);
            this.panel41.Controls.Add(this.set_print);
            this.panel41.Controls.Add(this.set_ccrconfig);
            this.panel41.Location = new System.Drawing.Point(0, 2);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(1616, 931);
            this.panel41.TabIndex = 0;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(1185, 463);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(390, 450);
            this.button11.TabIndex = 7;
            this.button11.Text = "Logs";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(800, 463);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(390, 450);
            this.button10.TabIndex = 6;
            this.button10.Text = "Logs";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(413, 463);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(390, 450);
            this.button9.TabIndex = 5;
            this.button9.Text = "Logs";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // set_logccr
            // 
            this.set_logccr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.set_logccr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.set_logccr.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.set_logccr.Location = new System.Drawing.Point(20, 463);
            this.set_logccr.Name = "set_logccr";
            this.set_logccr.Size = new System.Drawing.Size(390, 450);
            this.set_logccr.TabIndex = 4;
            this.set_logccr.Text = "Logs";
            this.set_logccr.UseVisualStyleBackColor = false;
            // 
            // set_maintenance
            // 
            this.set_maintenance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.set_maintenance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.set_maintenance.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.set_maintenance.Image = global::LaerMark_NMTkm21.Properties.Resources.repair;
            this.set_maintenance.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.set_maintenance.Location = new System.Drawing.Point(1185, 11);
            this.set_maintenance.Name = "set_maintenance";
            this.set_maintenance.Size = new System.Drawing.Size(390, 450);
            this.set_maintenance.TabIndex = 3;
            this.set_maintenance.Text = "Maintenance";
            this.set_maintenance.UseVisualStyleBackColor = false;
            this.set_maintenance.Click += new System.EventHandler(this.Set_maintenance_Click);
            // 
            // set_basemodel
            // 
            this.set_basemodel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.set_basemodel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.set_basemodel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.set_basemodel.Image = global::LaerMark_NMTkm21.Properties.Resources.analysis;
            this.set_basemodel.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.set_basemodel.Location = new System.Drawing.Point(798, 11);
            this.set_basemodel.Name = "set_basemodel";
            this.set_basemodel.Size = new System.Drawing.Size(390, 450);
            this.set_basemodel.TabIndex = 2;
            this.set_basemodel.Text = "Base Model Setting";
            this.set_basemodel.UseVisualStyleBackColor = false;
            this.set_basemodel.Click += new System.EventHandler(this.Set_basemodel_Click);
            // 
            // set_print
            // 
            this.set_print.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.set_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.set_print.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.set_print.Image = global::LaerMark_NMTkm21.Properties.Resources.print__4_;
            this.set_print.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.set_print.Location = new System.Drawing.Point(413, 11);
            this.set_print.Name = "set_print";
            this.set_print.Size = new System.Drawing.Size(390, 450);
            this.set_print.TabIndex = 1;
            this.set_print.Text = "Print Format / Print Setting";
            this.set_print.UseVisualStyleBackColor = false;
            this.set_print.Click += new System.EventHandler(this.Set_print_Click);
            // 
            // set_ccrconfig
            // 
            this.set_ccrconfig.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.set_ccrconfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.set_ccrconfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.set_ccrconfig.ForeColor = System.Drawing.Color.Black;
            this.set_ccrconfig.Image = global::LaerMark_NMTkm21.Properties.Resources.config;
            this.set_ccrconfig.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.set_ccrconfig.Location = new System.Drawing.Point(20, 9);
            this.set_ccrconfig.Name = "set_ccrconfig";
            this.set_ccrconfig.Size = new System.Drawing.Size(390, 450);
            this.set_ccrconfig.TabIndex = 0;
            this.set_ccrconfig.Text = "CCR / Internal Config";
            this.set_ccrconfig.UseVisualStyleBackColor = false;
            this.set_ccrconfig.Click += new System.EventHandler(this.Set_ccrconfig_Click);
            // 
            // ccr_internal
            // 
            this.ccr_internal.Controls.Add(this.panel42);
            this.ccr_internal.Location = new System.Drawing.Point(4, 22);
            this.ccr_internal.Name = "ccr_internal";
            this.ccr_internal.Size = new System.Drawing.Size(1616, 933);
            this.ccr_internal.TabIndex = 4;
            this.ccr_internal.Text = "ccr/internal";
            this.ccr_internal.UseVisualStyleBackColor = true;
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.panel42.Controls.Add(this.panel48);
            this.panel42.Controls.Add(this.panel46);
            this.panel42.Location = new System.Drawing.Point(0, 2);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(1616, 931);
            this.panel42.TabIndex = 0;
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel48.Controls.Add(this.dataGridView4);
            this.panel48.Controls.Add(this.panel49);
            this.panel48.Location = new System.Drawing.Point(826, 10);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(782, 911);
            this.panel48.TabIndex = 2;
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.internal_no,
            this.internal_param,
            this.internal_start,
            this.internal_length});
            this.dataGridView4.Location = new System.Drawing.Point(97, 132);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(640, 298);
            this.dataGridView4.TabIndex = 4;
            // 
            // internal_no
            // 
            this.internal_no.HeaderText = "No";
            this.internal_no.Name = "internal_no";
            // 
            // internal_param
            // 
            this.internal_param.HeaderText = "Parameter";
            this.internal_param.Name = "internal_param";
            this.internal_param.Width = 250;
            // 
            // internal_start
            // 
            this.internal_start.HeaderText = "Start";
            this.internal_start.Name = "internal_start";
            this.internal_start.Width = 125;
            // 
            // internal_length
            // 
            this.internal_length.HeaderText = "Length";
            this.internal_length.Name = "internal_length";
            this.internal_length.Width = 125;
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel49.Controls.Add(this.textBox83);
            this.panel49.Location = new System.Drawing.Point(3, 3);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(776, 72);
            this.panel49.TabIndex = 3;
            // 
            // textBox83
            // 
            this.textBox83.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.textBox83.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox83.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox83.ForeColor = System.Drawing.Color.White;
            this.textBox83.Location = new System.Drawing.Point(284, 20);
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new System.Drawing.Size(211, 24);
            this.textBox83.TabIndex = 0;
            this.textBox83.Text = "Internal Config";
            this.textBox83.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel46.Controls.Add(this.groupBox5);
            this.panel46.Controls.Add(this.dataGridView3);
            this.panel46.Controls.Add(this.panel47);
            this.panel46.Location = new System.Drawing.Point(7, 7);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(813, 914);
            this.panel46.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.ccr_bt_edit);
            this.groupBox5.Controls.Add(this.ccon_bt_save);
            this.groupBox5.Controls.Add(this.button8);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(138, 583);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(500, 197);
            this.groupBox5.TabIndex = 67;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Control Panel";
            // 
            // ccr_bt_edit
            // 
            this.ccr_bt_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.ccr_bt_edit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.ccr_bt_edit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.ccr_bt_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ccr_bt_edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ccr_bt_edit.ForeColor = System.Drawing.Color.White;
            this.ccr_bt_edit.Location = new System.Drawing.Point(85, 60);
            this.ccr_bt_edit.Name = "ccr_bt_edit";
            this.ccr_bt_edit.Size = new System.Drawing.Size(161, 78);
            this.ccr_bt_edit.TabIndex = 68;
            this.ccr_bt_edit.Text = "EDIT";
            this.ccr_bt_edit.UseVisualStyleBackColor = false;
            this.ccr_bt_edit.Click += new System.EventHandler(this.Ccr_bt_edit_Click);
            // 
            // ccon_bt_save
            // 
            this.ccon_bt_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.ccon_bt_save.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.ccon_bt_save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.ccon_bt_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ccon_bt_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.ccon_bt_save.ForeColor = System.Drawing.Color.White;
            this.ccon_bt_save.Location = new System.Drawing.Point(85, 60);
            this.ccon_bt_save.Name = "ccon_bt_save";
            this.ccon_bt_save.Size = new System.Drawing.Size(161, 78);
            this.ccon_bt_save.TabIndex = 66;
            this.ccon_bt_save.Text = "SAVE";
            this.ccon_bt_save.UseVisualStyleBackColor = false;
            this.ccon_bt_save.Visible = false;
            this.ccon_bt_save.Click += new System.EventHandler(this.Ccon_bt_save_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Red;
            this.button8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(286, 60);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(161, 78);
            this.button8.TabIndex = 65;
            this.button8.Text = "CANCEL";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.no_ccr,
            this.ccr_parameter,
            this.ccr_start,
            this.ccr_length,
            this.param_id,
            this.check});
            this.dataGridView3.Enabled = false;
            this.dataGridView3.Location = new System.Drawing.Point(36, 135);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(750, 287);
            this.dataGridView3.TabIndex = 1;
            // 
            // no_ccr
            // 
            this.no_ccr.HeaderText = "No";
            this.no_ccr.Name = "no_ccr";
            // 
            // ccr_parameter
            // 
            this.ccr_parameter.HeaderText = "Parameter";
            this.ccr_parameter.Name = "ccr_parameter";
            this.ccr_parameter.Width = 250;
            // 
            // ccr_start
            // 
            this.ccr_start.HeaderText = "CCR Start";
            this.ccr_start.Name = "ccr_start";
            this.ccr_start.Width = 125;
            // 
            // ccr_length
            // 
            this.ccr_length.HeaderText = "Length";
            this.ccr_length.Name = "ccr_length";
            this.ccr_length.Width = 125;
            // 
            // param_id
            // 
            this.param_id.HeaderText = "param_id";
            this.param_id.Name = "param_id";
            // 
            // check
            // 
            this.check.HeaderText = "Show in Table";
            this.check.Name = "check";
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel47.Controls.Add(this.textBox82);
            this.panel47.Location = new System.Drawing.Point(0, 3);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(810, 72);
            this.panel47.TabIndex = 3;
            // 
            // textBox82
            // 
            this.textBox82.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.textBox82.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox82.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox82.ForeColor = System.Drawing.Color.White;
            this.textBox82.Location = new System.Drawing.Point(284, 20);
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new System.Drawing.Size(211, 24);
            this.textBox82.TabIndex = 0;
            this.textBox82.Text = "CCR Config";
            this.textBox82.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // print_setting
            // 
            this.print_setting.Controls.Add(this.panel43);
            this.print_setting.Location = new System.Drawing.Point(4, 22);
            this.print_setting.Name = "print_setting";
            this.print_setting.Size = new System.Drawing.Size(1616, 933);
            this.print_setting.TabIndex = 5;
            this.print_setting.Text = "print_setting";
            this.print_setting.UseVisualStyleBackColor = true;
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.panel43.Controls.Add(this.panel52);
            this.panel43.Controls.Add(this.panel50);
            this.panel43.Location = new System.Drawing.Point(0, 1);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(1616, 931);
            this.panel43.TabIndex = 1;
            // 
            // panel52
            // 
            this.panel52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel52.Controls.Add(this.dataGridView5);
            this.panel52.Controls.Add(this.panel53);
            this.panel52.Location = new System.Drawing.Point(819, 3);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(794, 925);
            this.panel52.TabIndex = 5;
            // 
            // dataGridView5
            // 
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.printset_datablock,
            this.printset_source,
            this.printset_dataselect});
            this.dataGridView5.Location = new System.Drawing.Point(168, 143);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(493, 371);
            this.dataGridView5.TabIndex = 6;
            // 
            // printset_datablock
            // 
            this.printset_datablock.HeaderText = "Data Block";
            this.printset_datablock.Name = "printset_datablock";
            // 
            // printset_source
            // 
            this.printset_source.HeaderText = "Source";
            this.printset_source.Name = "printset_source";
            this.printset_source.Width = 150;
            // 
            // printset_dataselect
            // 
            this.printset_dataselect.HeaderText = "Data Selection";
            this.printset_dataselect.Name = "printset_dataselect";
            this.printset_dataselect.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.printset_dataselect.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.printset_dataselect.Width = 200;
            // 
            // panel53
            // 
            this.panel53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel53.Controls.Add(this.textBox85);
            this.panel53.Location = new System.Drawing.Point(3, 0);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(810, 72);
            this.panel53.TabIndex = 5;
            // 
            // textBox85
            // 
            this.textBox85.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.textBox85.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox85.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox85.ForeColor = System.Drawing.Color.White;
            this.textBox85.Location = new System.Drawing.Point(284, 20);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(211, 24);
            this.textBox85.TabIndex = 0;
            this.textBox85.Text = "Print Setting";
            this.textBox85.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(54)))), ((int)(((byte)(59)))));
            this.panel50.Controls.Add(this.pictureBox19);
            this.panel50.Controls.Add(this.panel51);
            this.panel50.Location = new System.Drawing.Point(3, 3);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(814, 925);
            this.panel50.TabIndex = 2;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Location = new System.Drawing.Point(66, 143);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(663, 309);
            this.pictureBox19.TabIndex = 5;
            this.pictureBox19.TabStop = false;
            // 
            // panel51
            // 
            this.panel51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel51.Controls.Add(this.textBox84);
            this.panel51.Location = new System.Drawing.Point(0, 0);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(814, 72);
            this.panel51.TabIndex = 4;
            // 
            // textBox84
            // 
            this.textBox84.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.textBox84.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox84.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox84.ForeColor = System.Drawing.Color.White;
            this.textBox84.Location = new System.Drawing.Point(284, 20);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(211, 24);
            this.textBox84.TabIndex = 0;
            this.textBox84.Text = "Print Format";
            this.textBox84.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // base_model_setting
            // 
            this.base_model_setting.Controls.Add(this.panel44);
            this.base_model_setting.Location = new System.Drawing.Point(4, 22);
            this.base_model_setting.Name = "base_model_setting";
            this.base_model_setting.Size = new System.Drawing.Size(1616, 933);
            this.base_model_setting.TabIndex = 6;
            this.base_model_setting.Text = "base_model_setting";
            this.base_model_setting.UseVisualStyleBackColor = true;
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.panel44.Controls.Add(this.button_datafrom_internal);
            this.panel44.Controls.Add(this.panel55);
            this.panel44.Controls.Add(this.bm_bt_save);
            this.panel44.Controls.Add(this.button3);
            this.panel44.Controls.Add(this.bm_bt_edit);
            this.panel44.Controls.Add(this.panel54);
            this.panel44.Controls.Add(this.dataGridView6);
            this.panel44.Location = new System.Drawing.Point(0, 1);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(1616, 931);
            this.panel44.TabIndex = 1;
            // 
            // button_datafrom_internal
            // 
            this.button_datafrom_internal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_datafrom_internal.Location = new System.Drawing.Point(60, 109);
            this.button_datafrom_internal.Name = "button_datafrom_internal";
            this.button_datafrom_internal.Size = new System.Drawing.Size(224, 44);
            this.button_datafrom_internal.TabIndex = 66;
            this.button_datafrom_internal.Text = "Data From Internal";
            this.button_datafrom_internal.UseVisualStyleBackColor = true;
            this.button_datafrom_internal.Click += new System.EventHandler(this.Button_datafrom_internal_Click);
            // 
            // panel55
            // 
            this.panel55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel55.Controls.Add(this.textBox87);
            this.panel55.Location = new System.Drawing.Point(534, 71);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(397, 72);
            this.panel55.TabIndex = 65;
            // 
            // textBox87
            // 
            this.textBox87.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.textBox87.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox87.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox87.ForeColor = System.Drawing.Color.White;
            this.textBox87.Location = new System.Drawing.Point(94, 24);
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(211, 24);
            this.textBox87.TabIndex = 0;
            this.textBox87.Text = "Data From CCR";
            this.textBox87.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bm_bt_save
            // 
            this.bm_bt_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.bm_bt_save.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.bm_bt_save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.bm_bt_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bm_bt_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.bm_bt_save.ForeColor = System.Drawing.Color.White;
            this.bm_bt_save.Location = new System.Drawing.Point(563, 764);
            this.bm_bt_save.Name = "bm_bt_save";
            this.bm_bt_save.Size = new System.Drawing.Size(203, 101);
            this.bm_bt_save.TabIndex = 64;
            this.bm_bt_save.Text = "SAVE";
            this.bm_bt_save.UseVisualStyleBackColor = false;
            this.bm_bt_save.Visible = false;
            this.bm_bt_save.Click += new System.EventHandler(this.Bm_bt_save_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Red;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(814, 764);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(203, 101);
            this.button3.TabIndex = 63;
            this.button3.Text = "CANCEL";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // bm_bt_edit
            // 
            this.bm_bt_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.bm_bt_edit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.bm_bt_edit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.bm_bt_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bm_bt_edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.bm_bt_edit.ForeColor = System.Drawing.Color.White;
            this.bm_bt_edit.Location = new System.Drawing.Point(563, 764);
            this.bm_bt_edit.Name = "bm_bt_edit";
            this.bm_bt_edit.Size = new System.Drawing.Size(203, 101);
            this.bm_bt_edit.TabIndex = 62;
            this.bm_bt_edit.Text = "EDIT";
            this.bm_bt_edit.UseVisualStyleBackColor = false;
            this.bm_bt_edit.Click += new System.EventHandler(this.Bm_bt_edit_Click);
            // 
            // panel54
            // 
            this.panel54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel54.Controls.Add(this.textBox86);
            this.panel54.Location = new System.Drawing.Point(3, 3);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(1610, 72);
            this.panel54.TabIndex = 5;
            // 
            // textBox86
            // 
            this.textBox86.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.textBox86.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox86.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox86.ForeColor = System.Drawing.Color.White;
            this.textBox86.Location = new System.Drawing.Point(641, 22);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(211, 24);
            this.textBox86.TabIndex = 0;
            this.textBox86.Text = "Base Model Setting";
            this.textBox86.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dataGridView6
            // 
            this.dataGridView6.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Enabled = false;
            this.dataGridView6.Location = new System.Drawing.Point(48, 173);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.Size = new System.Drawing.Size(1444, 535);
            this.dataGridView6.TabIndex = 2;
            // 
            // base_internal
            // 
            this.base_internal.Controls.Add(this.panel56);
            this.base_internal.Location = new System.Drawing.Point(4, 22);
            this.base_internal.Name = "base_internal";
            this.base_internal.Size = new System.Drawing.Size(1616, 933);
            this.base_internal.TabIndex = 8;
            this.base_internal.Text = "base_model_internal";
            this.base_internal.UseVisualStyleBackColor = true;
            // 
            // panel56
            // 
            this.panel56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.panel56.Controls.Add(this.button_datafrom_ccr);
            this.panel56.Controls.Add(this.panel57);
            this.panel56.Controls.Add(this.button12);
            this.panel56.Controls.Add(this.button13);
            this.panel56.Controls.Add(this.button14);
            this.panel56.Controls.Add(this.panel58);
            this.panel56.Controls.Add(this.dataGridView7);
            this.panel56.Location = new System.Drawing.Point(0, 1);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(1616, 931);
            this.panel56.TabIndex = 2;
            // 
            // button_datafrom_ccr
            // 
            this.button_datafrom_ccr.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_datafrom_ccr.Location = new System.Drawing.Point(60, 109);
            this.button_datafrom_ccr.Name = "button_datafrom_ccr";
            this.button_datafrom_ccr.Size = new System.Drawing.Size(224, 44);
            this.button_datafrom_ccr.TabIndex = 66;
            this.button_datafrom_ccr.Text = "Data From CCR";
            this.button_datafrom_ccr.UseVisualStyleBackColor = true;
            this.button_datafrom_ccr.Click += new System.EventHandler(this.Button_datafrom_ccr_Click);
            // 
            // panel57
            // 
            this.panel57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel57.Controls.Add(this.textBox88);
            this.panel57.Location = new System.Drawing.Point(534, 71);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(397, 72);
            this.panel57.TabIndex = 65;
            // 
            // textBox88
            // 
            this.textBox88.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.textBox88.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox88.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox88.ForeColor = System.Drawing.Color.White;
            this.textBox88.Location = new System.Drawing.Point(94, 24);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(211, 24);
            this.textBox88.TabIndex = 0;
            this.textBox88.Text = "Data From Internal";
            this.textBox88.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(563, 764);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(203, 101);
            this.button12.TabIndex = 64;
            this.button12.Text = "SAVE";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Visible = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Red;
            this.button13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(814, 764);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(203, 101);
            this.button13.TabIndex = 63;
            this.button13.Text = "CANCEL";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(57)))), ((int)(((byte)(108)))));
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(102)))), ((int)(((byte)(185)))));
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Location = new System.Drawing.Point(563, 764);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(203, 101);
            this.button14.TabIndex = 62;
            this.button14.Text = "EDIT";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // panel58
            // 
            this.panel58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.panel58.Controls.Add(this.textBox89);
            this.panel58.Location = new System.Drawing.Point(3, 3);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(1610, 72);
            this.panel58.TabIndex = 5;
            // 
            // textBox89
            // 
            this.textBox89.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(38)))), ((int)(((byte)(49)))));
            this.textBox89.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox89.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox89.ForeColor = System.Drawing.Color.White;
            this.textBox89.Location = new System.Drawing.Point(641, 22);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(211, 24);
            this.textBox89.TabIndex = 0;
            this.textBox89.Text = "Base Model Setting";
            this.textBox89.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dataGridView7
            // 
            this.dataGridView7.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.base_in_no,
            this.base_in_vehicle,
            this.base_in_eng,
            this.base_in_engcap,
            this.base_in_mission,
            this.base_in_axle,
            this.base_in_category,
            this.base_in_gvwr,
            this.base_in_gcw,
            this.base_in_gawr_fr,
            this.base_in_option,
            this.base_in_print});
            this.dataGridView7.Location = new System.Drawing.Point(48, 173);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.Size = new System.Drawing.Size(1495, 535);
            this.dataGridView7.TabIndex = 2;
            // 
            // base_in_no
            // 
            this.base_in_no.HeaderText = "No";
            this.base_in_no.Name = "base_in_no";
            // 
            // base_in_vehicle
            // 
            this.base_in_vehicle.HeaderText = "Vehicle";
            this.base_in_vehicle.Name = "base_in_vehicle";
            this.base_in_vehicle.Width = 125;
            // 
            // base_in_eng
            // 
            this.base_in_eng.HeaderText = "Eng Type";
            this.base_in_eng.Name = "base_in_eng";
            this.base_in_eng.Width = 125;
            // 
            // base_in_engcap
            // 
            this.base_in_engcap.HeaderText = "Eng Capacity";
            this.base_in_engcap.Name = "base_in_engcap";
            this.base_in_engcap.Width = 125;
            // 
            // base_in_mission
            // 
            this.base_in_mission.HeaderText = "Mission Type";
            this.base_in_mission.Name = "base_in_mission";
            this.base_in_mission.Width = 125;
            // 
            // base_in_axle
            // 
            this.base_in_axle.HeaderText = "AXLE Type";
            this.base_in_axle.Name = "base_in_axle";
            this.base_in_axle.Width = 125;
            // 
            // base_in_category
            // 
            this.base_in_category.HeaderText = "Category";
            this.base_in_category.Name = "base_in_category";
            this.base_in_category.Width = 125;
            // 
            // base_in_gvwr
            // 
            this.base_in_gvwr.HeaderText = "GVWR";
            this.base_in_gvwr.Name = "base_in_gvwr";
            this.base_in_gvwr.Width = 125;
            // 
            // base_in_gcw
            // 
            this.base_in_gcw.HeaderText = "GCW";
            this.base_in_gcw.Name = "base_in_gcw";
            this.base_in_gcw.Width = 125;
            // 
            // base_in_gawr_fr
            // 
            this.base_in_gawr_fr.HeaderText = "GAWR_RR";
            this.base_in_gawr_fr.Name = "base_in_gawr_fr";
            this.base_in_gawr_fr.Width = 125;
            // 
            // base_in_option
            // 
            this.base_in_option.HeaderText = "Option Spec Code";
            this.base_in_option.Name = "base_in_option";
            this.base_in_option.Width = 125;
            // 
            // base_in_print
            // 
            this.base_in_print.HeaderText = "Print Programe";
            this.base_in_print.Name = "base_in_print";
            // 
            // Maintenance
            // 
            this.Maintenance.Controls.Add(this.panel45);
            this.Maintenance.Location = new System.Drawing.Point(4, 22);
            this.Maintenance.Name = "Maintenance";
            this.Maintenance.Size = new System.Drawing.Size(1616, 933);
            this.Maintenance.TabIndex = 7;
            this.Maintenance.Text = "Maintenance";
            this.Maintenance.UseVisualStyleBackColor = true;
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.panel45.Location = new System.Drawing.Point(0, 1);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(1616, 931);
            this.panel45.TabIndex = 1;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.Timer2_Tick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(60, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(163, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "SELECT MODE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(120, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(578, 33);
            this.label4.TabIndex = 1;
            this.label4.Text = "LASER CERTIFICATION LABEL MACHINE";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(375, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 25);
            this.label5.TabIndex = 1;
            this.label5.Text = "CCR List";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(271, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(197, 25);
            this.label6.TabIndex = 2;
            this.label6.Text = "Printing Information";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(393, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 25);
            this.label7.TabIndex = 2;
            this.label7.Text = "CCR List";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(300, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(168, 25);
            this.label8.TabIndex = 3;
            this.label8.Text = "Print Information";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(1201, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(179, 25);
            this.label9.TabIndex = 34;
            this.label9.Text = "Machine Status  :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(63, 146);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 20);
            this.label10.TabIndex = 7;
            this.label10.Text = "Home";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(59, 146);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 20);
            this.label11.TabIndex = 8;
            this.label11.Text = "Printing";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(54, 145);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 20);
            this.label12.TabIndex = 8;
            this.label12.Text = "Checking";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.autotab.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.semiautotab.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.manualtab.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.setting.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.ccr_internal.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.panel48.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.panel49.ResumeLayout(false);
            this.panel49.PerformLayout();
            this.panel46.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.print_setting.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel52.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.panel53.ResumeLayout(false);
            this.panel53.PerformLayout();
            this.panel50.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.panel51.ResumeLayout(false);
            this.panel51.PerformLayout();
            this.base_model_setting.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel55.PerformLayout();
            this.panel54.ResumeLayout(false);
            this.panel54.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.base_internal.ResumeLayout(false);
            this.panel56.ResumeLayout(false);
            this.panel57.ResumeLayout(false);
            this.panel57.PerformLayout();
            this.panel58.ResumeLayout(false);
            this.panel58.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            this.Maintenance.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button_manual;
        private System.Windows.Forms.Button button_semi;
        private System.Windows.Forms.Button button_auto;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage autotab;
        private System.Windows.Forms.TabPage semiautotab;
        private System.Windows.Forms.TabPage manualtab;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button button_setting;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btlog_search;
        private System.Windows.Forms.TabPage setting;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Button set_ccrconfig;
        private System.Windows.Forms.Button set_maintenance;
        private System.Windows.Forms.Button set_basemodel;
        private System.Windows.Forms.Button set_print;
        private System.Windows.Forms.TabPage ccr_internal;
        private System.Windows.Forms.TabPage print_setting;
        private System.Windows.Forms.TabPage base_model_setting;
        private System.Windows.Forms.TabPage Maintenance;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridViewTextBoxColumn internal_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn internal_param;
        private System.Windows.Forms.DataGridViewTextBoxColumn internal_start;
        private System.Windows.Forms.DataGridViewTextBoxColumn internal_length;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.DataGridViewTextBoxColumn printset_datablock;
        private System.Windows.Forms.DataGridViewTextBoxColumn printset_source;
        private System.Windows.Forms.DataGridViewComboBoxColumn printset_dataselect;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.Button set_logccr;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Button bm_bt_save;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button bm_bt_edit;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.Button button_datafrom_internal;
        private System.Windows.Forms.TabPage base_internal;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Button button_datafrom_ccr;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_vehicle;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_eng;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_engcap;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_mission;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_axle;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_category;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_gvwr;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_gcw;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_gawr_fr;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_option;
        private System.Windows.Forms.DataGridViewTextBoxColumn base_in_print;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button ccon_bt_save;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button ccr_bt_edit;
        private System.Windows.Forms.DataGridViewTextBoxColumn no_ccr;
        private System.Windows.Forms.DataGridViewTextBoxColumn ccr_parameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ccr_start;
        private System.Windows.Forms.DataGridViewTextBoxColumn ccr_length;
        private System.Windows.Forms.DataGridViewTextBoxColumn param_id;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}

